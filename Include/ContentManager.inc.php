<?php
/* --- TMP --- */
function parseAndPutsTpl()
{
    $tplConfig = cmsConfig(array('tplName', 'Template'));
    $tplGetCtt = dataConfig(array('view_fd', 'template_fd'));
    $tplGetCtt = APP_ROOT.$tplGetCtt['view_fd'].$tplGetCtt['template_fd'].$tplConfig['tplName'];
    $tplConfig = $tplConfig['Template'][$tplConfig['tplName']];
    $contentJo = jsonLoad('Content');
    $currentPg = currentPage();

    if (!isset($tplConfig['files'][$currentPg])) {
        $tplGetCtt .= '/'.$tplConfig['files']['main'];
    } else {
        $tplGetCtt .= '/'.$tplConfig['files'][$currentPg];
    }

    $tplGetCtt = file_get_contents($tplGetCtt, null, null);

    foreach ($tplConfig['Scheme'] as $key => &$value) {
        $tplGetCtt = str_replace('[M:'.$key.']', '<?php echo '.$value.' ?>', $tplGetCtt);
    }

    foreach ($tplConfig['Config'] as $key => &$value) {
        $tplGetCtt = str_replace('[C:'.$key.']', $value, $tplGetCtt);
    }

    echo eval('?>'.$tplGetCtt);
}

function cmsGetGlobalData()
{
    $cmsConfig  = cmsConfig(array('serverUrl', 'defaultLanguage', 'defaultCharset', 'tplName', 'webName', 'webDescription'));
    $template_fd = dataConfig('template_fd');

    $cmsConfig['tpl_url'] = $cmsConfig['serverUrl'].$template_fd.$cmsConfig['tplName'].'/';
    $cmsConfig['header'] = '<meta http-equiv="content-type" content="text/html; charset='.$cmsConfig['defaultCharset'].'">
        <title>'.$cmsConfig['webName'].': '.currentPage().'</title>
        <meta name="description" content="'.$cmsConfig['webDescription'].'">
        ';

    return $cmsConfig;
}

/* ---------- Content checker ---------- */
function currentPage()
{
    if (isset($_GET['Page'])) {
        $current = securityParser($_GET['Page']);
    } else {
        $current = 'Accueil';
    }

    return ($current);
}

function contentLanguage($arg)
{
    if (isset($_SESSION['LGG']) && array_key_exists($_SESSION['LGG'], $arg)) {
        $arg = $_SESSION['LGG'];
    } else {
        $arg = cmsConfig('defaultLanguage');
    }

    return ($arg);
}

function checkLanguage()
{
    $buff      = '';
    $cmsConfig = cmsConfig(array('supportedLanguage', 'defaultLanguage'));

    if (isset($_GET['LGG'])) {
        $buff = securityParser(substr($_GET['LGG'], 0, 5), true);

        if (in_array($buff, $cmsConfig['supportedLanguage'], true)) {
            $_SESSION['LGG'] = $buff;
        } else {
            $_SESSION['LGG'] = $cmsConfig['defaultLanguage'];
        }
    } else {
        if (!isset($_SESSION['LGG'])) {
            if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
                $buff    = strtolower(securityParser(substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 5), true));

                if (in_array($buff, $cmsConfig['supportedLanguage'], true)) {
                    $_SESSION['LGG'] = $buff;
                } else {
                    $compare = tinyCompareLanguage(substr($buff, 0, 2), $cmsConfig['supportedLanguage']);

                    if ('' !== $compare) {
                        $_SESSION['LGG'] = $compare;
                    } else {
                        $_SESSION['LGG'] = $cmsConfig['defaultLanguage'];
                    }
                }
            } else {
                $_SESSION['LGG'] = $cmsConfig['defaultLanguage'];
            }
        }
    }
}

function tinyCompareLanguage($string, $arrLg)
{
    foreach ($arrLg as &$value) {
        if (substr($value, 0, 2) === $string) {
            return $value;
        }
    }

    return '';
}

/* ---------- Json manager ---------- */
function jsonLoad($dbRead)
{
    static $ext;
    static $path;

    static $Config;
    static $Content;
    static $Errors;
    static $Guest;
    static $Plugins;

    if (null === $path) {
        $ext  = dataConfig('database_ext');
        $path = APP_ROOT.dataConfig('database_fd');
    }

    if (null === $$dbRead) {
        $$dbRead = jsonRead($path.$dbRead.$ext);
    }

    return (${$dbRead});
}

function jsonRead($arg)
{
    $request = $arg;

    if (is_file($arg)) {
        $arg = file_get_contents($arg, null, null);
        $arg = json_decode($arg, true);

        if (json_last_error()) {
            exit('Error: '.json_last_error().' - Json: '.$request);
        }
    } else {
        exit('Error: One json file is not found or not accessible.');
    }

    return ($arg);
}

function pluginsDb()
{
    $dirConfig = strlen(APP_ROOT.dataConfig('plugins_fd'));

    $requestBy = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, 1)['0']['file'];
    $requestBy = substr($requestBy, $dirConfig);
    $requestBy = substr($requestBy, 0, strpos($requestBy, '/'));

    return (jsonLoad('Plugins')[$requestBy]['DataBase']['Used']);
}

/* ---------- Php content generator ---------- */
function execPhpContent(&$content, $nTime)
{
    while (0 < $nTime) {
        --$nTime;

        $pref = strpos($content, '!PHP:') + 5;
        $suff = strpos($content, ':PHP!') - $pref;
        $find = substr($content, $pref, $suff);

        $exec = 'return ('.$find.');';
        $exec = eval($exec);

        $content = substr_replace($content, $exec, $pref - 5, $suff + 10);
    }
}

/* ---------- Plugins check and load ---------- */
function pluginsManager()
{
    static $pluginsLoading = 1;

    if ($pluginsLoading) {
        $pluginsLoading = 0;
        $pluginsList    = jsonLoad('Plugins');
        $pgLocation     = APP_ROOT.dataConfig('plugins_fd');

        foreach ($pluginsList as $key => &$value) {
            if ($pluginsList[$key]['Activate']) {
                require $pgLocation.$key.'/'.$key.'.plug.php';
            }
        }
    }

    return $pluginsLoading;
}

/* ---------- Return content ---------- */
function returnPage($pageContent, $page, $type = null, $execPhp = 1) // $type, must be Title or Text
{
    $pageContent = $pageContent['Pages'];

    if (false === strpos($page, '/')) {
        $pageContent = (isset($pageContent[$page])) ? $pageContent[$page] : false;
    } else {
        foreach (explode("/", $page) as &$value) {
            $pageContent = (isset($pageContent[$value])) ? $pageContent[$value] : false;
        }
    }
    
    if ($pageContent) {
        $language = contentLanguage($pageContent['title']);

        if ('Title' === $type) {
            $outPut = $pageContent['title'][$language];
        } else {
            $outPut = dataConfig(array('content_fd', 'content_ext'));
            $outPut = APP_ROOT.$outPut['content_fd'].$page.'/'.$language.$outPut['content_ext'];
            $outPut = file_get_contents($outPut);

            if ($execPhp && false !== strpos($outPut, '!PHP:')) {
                execPhpContent($outPut, substr_count($outPut, '!PHP:'));
            }
        }
    } else {
        if ('Title' === $type) {
            $outPut = 'Keep calm ! May the 404 page be with you !';
        } else {
            $outPut = declareError($page, '404')."\n";
        }
    }

    return ($outPut);
}

function returnMenu($menuContent, $cat, $depth = -1, $tag = 'ul') // $type, must be 'ul', 'ol' and 'menu'
{
    $root = "";

    if (false === strpos($cat, '/')) {
        $menuContent = (isset($menuContent[$cat])) ? $menuContent[$cat] : false;
    } else {
        $i = 0;

        foreach (explode("/", $cat) as &$value) {
            $menuContent = (isset($menuContent[$value])) ? $menuContent[$value] : false;
           
            // For include root patch to url, but not the first cat (page, extra, ...)
            if (0 !== $i) {
                if ("" === $root) {
                    $root .= $value;
                } else {
                    $root .= '/'.$value;
                }
            }

            ++$i;
        }
    }

    if (false !== $menuContent) {
        $outPut  = '<'.$tag.'>';
        $outPut .= arrayMenuParser($outPut, $menuContent, $depth, $tag, $root);
        $outPut .= '</'.$tag.'>'."\n";
    } else {
        $outPut = declareError('Menu => '.$cat, 'Nfound')."\n";
    }

    return ($outPut);
}

function arrayMenuParser(&$outPut, &$arg, &$depth, &$tag, $root)
{
    if (0 === $depth) {
        return null;
    } elseif (0 < $depth) {
        --$depth;
    }

    foreach ($arg as $key => &$value) {
        if (isset($value['title'])) {
            $key      = ("" === $root) ? $key : $root.'/'.$key;
            $link     = (isset($value['link'])) ? $value['link'] : '?Page='.$key;
            $language = contentLanguage($value['title']);

            if (false !== strpos($key, '/')) {
                $keyId = str_replace('/', '-', $key);
            } else {
                $keyId = $key;
            }

            if ($value['category']) {
                $outPut .= '<li id="'.$keyId.'" class="category">';

                if (isset($value['andAPage'])) {
                    $outPut .= '<a href="'.$link.'"><span>'.$value['title'][$language].'</span></a><'.$tag.'>';
                } else {
                    $outPut .= '<span>'.$value['title'][$language].'</span><'.$tag.'>';
                }

                $outPut .= arrayMenuParser($outPut, $value, $depth, $tag, $key);
                $outPut .= '</'.$tag.'></li>';
            } else {
                $outPut .= '<li id="'.$keyId.'" class="link"><a ';
                $outPut .= (isset($value['blank']) && $value['blank']) ? 'target="_blank" ' : '' ;
                $outPut .= 'href="'.$link.'">'.$value['title'][$language].'</a></li>';
            }
        }
    }
}
