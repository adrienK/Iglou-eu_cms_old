<?php
function main()
{

    // Declare config
    $cmsConfig = cmsConfig(array('debugMode', 'onlyHTTPS', 'defaultCharset'));

    // Add php charset
    header('Content-Type: text/html;charset='.$cmsConfig['defaultCharset']);

    // Disable GC
    if (gc_enabled()) {
        gc_collect_cycles();
        gc_disable();
    }

    // Check php version and lib
    if ($cmsConfig['debugMode']) {
        phpChecker();
    }

    // Check HTTPS
    if ($cmsConfig['onlyHTTPS'] && empty($_SERVER["HTTPS"]) && strtolower($_SERVER["HTTPS"]) !== "on") {
        exit('DIE : Only HTTPS authorized');
    }

    // Cookies
    if ($cmsConfig['onlyHTTPS']) {
        sessionStart(true);
    } else {
        sessionStart();
    }

    // Check language
    checkLanguage();

    // Load Plugins
    pluginsManager();

    // Tpl include
    parseAndPutsTpl();
}
