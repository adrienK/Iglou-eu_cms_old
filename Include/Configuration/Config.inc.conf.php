<?php
function cmsConfig($arg, $change = 0, $newValue = null)
{
    static $cmsConfig;

    if (null === $cmsConfig) {
        $cmsConfig = jsonLoad('Config');

        if ($cmsConfig['onlyHTTPS']) {
            $cmsConfig['serverUrl'] = 'https://'.$cmsConfig['serverUrl'];
        } else {
            $cmsConfig['serverUrl'] = 'http://'.$cmsConfig['serverUrl'];
        }
    }

    if ($change) {
        $cmsConfig[$arg] = $newValue;
        $arg  = $cmsConfig[$arg];
    } elseif ((array) $arg === $arg) {
        foreach ($arg as &$value) {
            if (isset($cmsConfig[$value])) {
                $foreach[$value] = $cmsConfig[$value];
            } else {
                $foreach[$value] = 0;
            }
        }

        $arg = &$foreach;
    } else {
        if (isset($cmsConfig[$arg])) {
            $arg = $cmsConfig[$arg];
        } else {
            $arg = 0;
        }
    }

    return ($arg);
}

function dataConfig($arg)
{
    static $view_fd      = 'Public/';

    static $template_fd  = 'Templates/';
    static $plugins_fd   = 'Plugins/';
    static $database_fd  = 'Database/';
    static $content_fd   = 'Content/';
    
    static $database_ext = '.json';
    static $content_ext  = '.txt';

    if ((array) $arg === $arg) {
        foreach ($arg as &$value) {
            if (isset($$value)) {
                $foreach[$value] = ${$value};
            } else {
                $foreach[$value] = 0;
            }
        }

        $arg = &$foreach;
    } else {
        if (isset($$arg)) {
            $arg = ${$arg};
        } else {
            $arg = 0;
        }
    }

    return $arg;
}
