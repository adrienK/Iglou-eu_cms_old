<?php
function declareError($arg, $err)
{
    $outPut = jsonLoad('Errors');

    switch ($err) {
        case '404':
            $outPut = $outPut[$err][contentLanguage($outPut[$err])];
            $outPut = str_replace('[:PAGE_VAR:]', $arg, $outPut);
            break;
        case 'Nfound':
            $outPut = $outPut[$err][contentLanguage($outPut[$err])];
            $outPut = str_replace('[:NFOUND:]', $arg, $outPut);
            break;
        default:
            exit('ErrorsManager.inc.php => Bad type error on '.$err);
            break;
    }

    return ($outPut);
}

function phpChecker()
{
    $actualPhp = phpversion();

    if (version_compare($actualPhp, REQUIRED_PHP_VERSION, '<')) {
        exit('Ouups => '.$actualPhp.', Your php version is too old. This script require PHP V: '.REQUIRED_PHP_VERSION.' or newer, sorry.');
    }
}

function timeDB()
{
    $time = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    $time = round($time, 4);

    return ($time);
}

function memoryDB()
{
    $memory = memory_get_usage() / 1048576;
    $memory = round($memory, 3);

    return ($memory);
}
