# Iglou.eu
*Ancien projet de CMS pour le site principal.*

---
### En
- Your Virtual Host, should point to */Public/ ,
- For only use */Public/index.php.
- Only one conf file and one template, for use this engine.
- ConfFile: Include/Configuration/Config.inc.conf.php
- Template: Public/Templates/*

### FR
- Votre hôte virtuel doit pointer sur */public/,
- Pour utiliser uniquement */public/index.php.
- Un seul fichier de conf et un modèle, pour l'utilisation de ce moteur.
- ConfFile: Include/Configuration/Config.inc.conf.php
- Modèle: Public/Templates/*
