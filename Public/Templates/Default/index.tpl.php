<?php
defined('APP_ROOT') or die;
$data = cmsGetGlobalData();
?>

<!doctype html>
<html lang="fr">
    <head>
        <?php echo $data['header']; ?>
        <link rel="stylesheet" href="<?php echo $data['tpl_url']; ?>Css/normalize.css">
    </head>
    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser.
        Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div id=header>
            [M:module2]
        </div>
        <div id=contens style="background: [C:bg_color];">
            [M:module3]
        </div>
        <div id=footer>
            <?php echo timeDB(),'secondes - ',memoryDB(),'mo'; ?>
        </div>
    </body>
</html>
